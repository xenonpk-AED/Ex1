package Ex1;

import static java.lang.System.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author xenonpk
 */
public class Counter {

    private int value;

    public Counter(int startv) {
        value = startv;
    }

    public void resetCounter() {
        value = 0;
    }

    public int getValue() {
        return (value);
    }

    public void increment() {
        value++;
    }

    public void increment(int val) {
        value += val;
    }

    public void decrement() {
        if (value != 0) {
            value--;
        }
    }

    public void decrement(int val) {
        if (value < val) {
            resetCounter();
        } else {
            value -= val;
        }
    }

    @Override
    public Counter clone() {
        return (new Counter(value));
    }

    @Override
    public boolean equals(Object a) {
        Counter b = (Counter) a;
        if (b == null || getClass() != b.getClass()) {
            return false;
        }
        return value == b.getValue();
    }

    @Override
    public String toString() {
        String v = "[" + value + "]";
        return (v);
    }
}
